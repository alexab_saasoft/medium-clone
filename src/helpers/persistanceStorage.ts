export const getItem = (key: string): string | null => {
  try {
    return JSON.parse(localStorage.getItem(key) || '');
  } catch (e) {
    return null;
  }
};

export const setItem = (key: string, data: unknown) => {
  try {
    localStorage.setItem(key, JSON.stringify(data));
  } catch (e) {
    console.log('Error saving data in localstorage', e);
  }
};

export const removeItem = (key: string) => {
  try {
    localStorage.removeItem(key);
  } catch (e) {
    console.log('Error saving data in localstorage', e);
  }
};