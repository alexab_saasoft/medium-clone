import { defineStore } from 'pinia';
import { Comment } from '@/entities/comment';
import commentsApi from '@/api/comments';
import { LoadingState } from '@/stores/types';

export const useCommentsStore = defineStore('comments', {
  state: (): LoadingState<Array<Comment>> => ({
    data: null,
    isLoading: false,
    error: null
  }),
  actions: {
    async getComments({ slug }: {slug: string}) {
      try {
        this.$patch({
          isLoading: true,
          data: null
        });
        const comments: Array<Comment> = await commentsApi.getComments(slug);
        this.$patch({
          isLoading: true,
          data: comments.sort((a: Comment, b: Comment): number => {
            return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime();
          })
        });
      } catch (e) {
        this.$reset();
        throw e;
      }
    },
    async addComment(data: { slugArticle: string, comment: Comment }) {
      try {
        const comment: Comment = await commentsApi.addComment(data);
        this.$patch((state) => {
          state.data?.unshift(comment);
        });
      } catch (e: any) {
        this.$patch({
          error: e
        });
        throw e;
      }
    },
    async deleteComment(data: { slugArticle: string, slugComment: string }) {
      try {
        await commentsApi.deleteComment(data);
        this.$patch((state) => {
          if (state.data != null) {
            state.data = state.data.filter((x: Comment) => x.id.toString() != data.slugComment);
          }
        });
      } catch (e) {
        console.error(e);
        throw e;
      }
    }
  }
});