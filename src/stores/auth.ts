import { defineStore } from 'pinia';
import { UserProfile } from '@/entities/user';
import { saveToken, getToken, destroyToken } from '@/service/jwtService';

import authApi from '@/api/auth';
import userProfileApi from '@/api/userProfile';
import { useSettingsStore } from './settings';
import { UserViewModel } from '@/models/user';

export interface AuthState {
  isSubmitting: boolean,
  isLoading: boolean,
  currentUser: UserProfile | null,
  validationErrors: null | any,
  isLoggedIn: boolean | null
}

export const useAuthUserStore = defineStore('auth', {
  state: (): AuthState => ({
    isSubmitting: false,
    isLoading: false,
    currentUser: null,
    validationErrors: null,
    isLoggedIn: null
  }),
  getters: {
    isAnonymous: (state): boolean => {
      return state.isLoggedIn === null && getToken() == null;
    }
  },
  actions: {
    async login(credentials: UserViewModel): Promise<void> {
      try {
        this.$patch({
          validationErrors: null,
          isSubmitting: true
        } as AuthState);

        const user = await authApi.login(credentials);
        // TODO: api присылает некорректную картинку в текущем юзере
        if (user.bio === user.image) {
          const userWithImage = await userProfileApi.getUserProfile(user.username);
          user.image = userWithImage.image;
        }
        saveToken(user.token as string);
        this.updateCurrentUser(user);
      } catch (e: any) {
        this.$patch({
          isSubmitting: false,
          validationErrors: e.response.data.errors
        });
        throw e;
      }
    },
    async logout(): Promise<void> {
      try {
        destroyToken();
        this.$reset();
      } catch (e) {
        console.log(e);
        throw e;
      }
    },
    async getCurrentUser(): Promise<void> {
      try {
        this.$patch({
          isLoading: true
        });
        const user = await authApi.getCurrentUser();
        // TODO: api присылает некорректную картинку в текущем юзере
        if (user.bio === user.image) {
          const userWithImage = await userProfileApi.getUserProfile(user.username);
          user.image = userWithImage.image;
        }
        this.updateCurrentUser(user);
      } catch (e) {
        this.$reset();
        throw e;
      }
    },
    async updateCurrentUserProfile(currentUser: UserProfile): Promise<void> {
      const settingsStore = useSettingsStore();
      try {
        settingsStore.$patch({
          isSubmitting: true,
          validationErrors: null
        });
        const user = await authApi.updateCurrentUserProfile(currentUser);
        this.updateCurrentUser(user);
        settingsStore.$patch({
          isSubmitting: false,
        });
      } catch (e: any) {
        settingsStore.$patch({
          isSubmitting: false,
          validationErrors: e.response.data.errors
        });
        this.$patch({
          isSubmitting: false,
          validationErrors: e.response.data.errors
        });
        throw e;
      }
    },
    updateCurrentUser(user: UserProfile) {
      this.$patch({
        currentUser: user,
        isLoading: false,
        isLoggedIn: true,
      });
    }
  }
});