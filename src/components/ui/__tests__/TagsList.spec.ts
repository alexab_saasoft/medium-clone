import { render } from '@testing-library/vue';
import { Tag } from '@/entities/tag';
import TagsListVue from '@/components/ui/TagsList.vue';

describe('Tags List', () => {
  const tag: Tag = new Tag();

  it('renders tagsList', () => {
    const tags: Tag[] = [tag];
    render(TagsListVue, {
      props: {
        tags
      }
    });
  });
});
