import axios from "@/plugins/axios";
import { UserProfile } from "@/entities/user";
import { UserViewModel } from "@/models/user";

const login = (user: UserViewModel): Promise<UserProfile> => {
  return axios
    .post('/users/login', { user })
    .then(res => res.data.user);
};

const getCurrentUser = (): Promise<UserProfile> => {
  return axios
    .get('/user')
    .then(res => res.data.user);
};

const updateCurrentUserProfile = (user: UserProfile): Promise<UserProfile> => {
  return axios
    .put('/user', { user })
    .then(res => res.data.user);
};

export default {
  login,
  getCurrentUser,
  updateCurrentUserProfile,
};