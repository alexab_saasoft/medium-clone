# Клон сайта Medium

- Сайт предназначен для публикации и просмотра постов.
- Для написания использованны VueCli и Vue 3 (Composition API) + TypeScript, синхронизация данных происходит по REST API.
- Для тестов использовались библиотеки Jest + Testing Library + TypeScript.
- Настроено CI/CD с автоматической сборкой, прогоном тестов и деплоем на firebase.

- Фичи приложения:
1. Регистрация и авторизация с сохранением состояния текущего пользователя.
2. Редактирования данных пользователя, а так же просмотр информации о других пользователях.
3. Добавление новых и редактирование уже существующих постов.
4. Просмотр постов (общих, своих, по тегам).
5. Возможность проставлять лайки понравившимся публикациям.
6. Возможность добавлять и удалять комментарии.

- посмотреть приложение можно по ссылке https://medium-clone-58266.web.app

Если нет желания регистрироваться, моковые данные:

* Логин: a@mail.ru
* Пароль: a@mail.ru
